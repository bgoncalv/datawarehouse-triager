"""Init."""
from cki_lib.session import get_session
from datawarehouse import Datawarehouse

import settings

session = get_session('cki.triager')
dw_client = Datawarehouse(
    settings.DATAWAREHOUSE_URL, settings.DATAWAREHOUSE_TOKEN
)
