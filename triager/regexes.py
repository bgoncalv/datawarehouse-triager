"""Regexes to look for bugs 🐛🐛."""
from contextlib import contextmanager
from functools import lru_cache
import re
import time

from cki_lib.logger import get_logger
import prometheus_client as prometheus

import settings

from . import dw_client

LOGGER = get_logger('cki.triager.regexes')
METRIC_REGEX_SEARCH_TIME = prometheus.Histogram(
    'regex_search_time_seconds',
    'Time spent looking through a log file with the regexes'
)
METRIC_REGEX_MATCH_TIME = prometheus.Summary(
    'regex_match_time_seconds',
    'Time spent matching, grouped by regex.',
    ['regex_id']
)


class RegexChecker:
    # pylint: disable=too-few-public-methods
    """RegexChecker."""

    def __init__(self):
        """Init."""
        self.lookups = []

    @staticmethod
    def _compile_lookups(lookups):
        """Compile the regexes."""
        LOGGER.debug('Compiling lookups')
        compiled_lookups = []
        for lookup in lookups:
            for field_name in ('text_match', 'test_name_match', 'file_name_match'):
                field = getattr(lookup, field_name)
                compiled_field = re.compile(field, re.DOTALL) if field else None
                setattr(lookup, f'compiled_{field_name}', compiled_field)

            compiled_lookups.append(lookup)

        return compiled_lookups

    @staticmethod
    def _get_cache_ttl(duration=300):
        """Generate a hash to invalidate the cached call signature."""
        return round(time.time() / duration)

    def download_lookups(self):
        """Call self._download_lookups with cache hash."""
        self._download_lookups(ttl_hash=self._get_cache_ttl())

    @lru_cache(maxsize=1)
    def _download_lookups(self, ttl_hash):
        """Download and compile regexes from Datawarehouse."""
        del ttl_hash  # Not used, just a param to invalidate lru_cache
        LOGGER.debug('Downloading lookups')
        self.lookups = self._compile_lookups(
            dw_client.issue_regex.list()
        )
        LOGGER.debug('Found %i lookups', len(self.lookups))

    @staticmethod
    def _match(lookup, text, log, obj):
        """Match lookup against log."""
        text_match = lookup.compiled_text_match
        test_name_match = lookup.compiled_test_name_match
        file_name_match = lookup.compiled_file_name_match

        # Note: Fix it better. Description not existing breaks on received messages
        # (work fine with api)
        try:
            description = obj.description
        except KeyError:
            description = ''

        if test_name_match and not test_name_match.search(description or ''):
            return False

        if file_name_match and not file_name_match.search(log['name']):
            return False

        if not text_match.search(text):
            return False

        # Fallback. Found it.
        return True

    @staticmethod
    @contextmanager
    def _match_time_measure(lookup):
        """Measure the time matching a regex."""
        start = time.time()
        try:
            yield
        finally:
            elapsed = time.time() - start
            METRIC_REGEX_MATCH_TIME.labels(regex_id=lookup.id).observe(elapsed)
            if elapsed > 1:
                # Arbitrary value, tuning might be necessary
                LOGGER.error("Regex matching took too long. regex_id=%d elapsed_s=%f",
                             lookup.id, elapsed)

    @METRIC_REGEX_SEARCH_TIME.time()
    def search(self, text, log, obj):
        """Search for regexes ocurrences on text."""
        issues = []
        LOGGER.debug('Searching in %s', log['url'])
        for lookup in self.lookups:
            with self._match_time_measure(lookup):
                if not self._match(lookup, text, log, obj):
                    continue

            result = {'name': lookup.issue['description'], 'id': lookup.issue['id']}
            LOGGER.debug('Found %s', result)
            issues.append(result)

        return issues or settings.NOT_FOUND
