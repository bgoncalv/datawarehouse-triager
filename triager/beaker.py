"""Beaker checker."""
from functools import lru_cache

from cki_lib.logger import get_logger

from triager import session

LOGGER = get_logger('cki.triager.beaker')


class BeakerChecker:
    """BeakerChecker class."""

    def __init__(self, url):
        """Init."""
        self.url = url

    @lru_cache(maxsize=None)
    def get_recipe(self, recipe_id):
        """Get recipe data."""
        url = f'{self.url}/recipes/{recipe_id}'
        response = session.get(url, headers={'Accept': 'application/json'})
        return response.json()

    @staticmethod
    def _search_task_in_recipe(recipe, task_id):
        """Iterate over the tasks and return a particular task."""
        for task in recipe['tasks']:
            if task['id'] == task_id:
                return task
        return None

    def get_task(self, recipe_id, task_id):
        """Get task."""
        recipe = self.get_recipe(recipe_id)
        task = self._search_task_in_recipe(recipe, task_id)
        return task

    @staticmethod
    def external_watchdog_expired(task):
        """Check if external watchdog expired in task."""
        if task['name'] == 'Boot test':
            # Don't report boot failures as Watchdog issues.
            return False

        for result in task['results']:
            if result['message'] == 'External Watchdog Expired':
                return True

        return False

    @staticmethod
    def local_watchdog_expired(task):
        """Check if local watchdog expired in task."""
        for result in task['results']:
            if result['path'] == '/10_localwatchdog' and \
                    result['result'] == 'Warn' and \
                    task['result'] == 'Warn':
                return True

        return False

    @staticmethod
    def watchdog_expired(task):
        """Check if any watchdog expired in task."""
        if BeakerChecker.local_watchdog_expired(task):
            return True

        if BeakerChecker.external_watchdog_expired(task):
            return True

        return False

    def check_testrun(self, test):
        """Check if test expired watchdogs."""
        recipe_id = test.misc['beaker']['recipe_id']
        task_id = test.misc['beaker']['task_id']

        task = self.get_task(recipe_id, task_id)

        return self.watchdog_expired(task)
