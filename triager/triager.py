"""Triager module."""
from cki_lib.logger import get_logger
import prometheus_client as prometheus
from restclient import APIManager

import settings
from triager import dw_client
from triager.checkers import BuildFailureChecker
from triager.checkers import TestFailureChecker

LOGGER = get_logger('cki.triager.triager')

METRIC_MESSAGE_PROCESS_TIME = prometheus.Summary(
    'message_process_time_seconds', 'Time spent processing job')


class DWObject:
    # pylint: disable=too-few-public-methods
    """Mock Datawarehouse api lib from a dict."""

    def __init__(self, obj_type, attrs):
        """Init."""
        self.type = obj_type
        self.attrs = attrs

    def __getattr__(self, name):
        """Override getattr to treat dict elements as object attributes."""
        try:
            return self.attrs[name]
        except KeyError:
            return self.__dict__.get(name)

    def issues_create(self, issue_id):
        """Create issue for the object."""
        obj_iid = self.attrs['misc']['iid']

        api = APIManager(settings.DATAWAREHOUSE_URL, settings.DATAWAREHOUSE_TOKEN)
        api.post(f'/api/1/kcidb/{self.type}s/{obj_iid}/issues', {'issue_id': issue_id})


class Triager:
    """Triage a kcidb object."""

    def __init__(self, dry_run=False):
        """Init."""
        self.dry_run = dry_run

    @staticmethod
    def check_revision(revision):
        """Check revision for known issues."""
        LOGGER.debug('Checking revision id=%s', revision.id)
        return []

    @staticmethod
    def check_build(build):
        """Check build for known issues."""
        LOGGER.debug('Checking build id=%s', build.id)
        issues = []

        if build.valid:
            return []

        issues_found = BuildFailureChecker.check(build)
        if issues_found:
            issues.extend(
                [(build, issue) for issue in issues_found]
            )

        return issues

    @staticmethod
    def check_test(test):
        """Check test for known issues."""
        LOGGER.debug('Checking test id=%s', test.id)
        issues = []

        if test.status == 'PASS':
            return []

        issues_found = TestFailureChecker.check(test)
        if issues_found:
            issues.extend(
                [(test, issue) for issue in issues_found]
            )

        return issues

    def report_issues(self, issues):
        """Report a list of (object, issue)."""
        for obj, issue in issues:
            LOGGER.info('Linking issue id=%s to id=%s', issue, obj.id)

            if self.dry_run:
                LOGGER.info('Dry run. Skipping issue reporting.')
                continue

            method = obj.issues_create if isinstance(obj, DWObject) else obj.issues.create
            method(issue_id=issue['id'])

    @METRIC_MESSAGE_PROCESS_TIME.time()
    def check(self, obj_type, obj):
        """Check object for issues."""
        methods = {
            'revision': self.check_revision,
            'build': self.check_build,
            'test': self.check_test,
        }

        if isinstance(obj, str):
            # Treat obj as the id and query for the object.
            obj = getattr(dw_client.kcidb, f'{obj_type}s').get(obj)
        elif isinstance(obj, dict):
            # Mock datawarehouse lib to avoid extra API calls.
            obj = DWObject(obj_type, obj)

        issues = methods[obj_type](obj)
        self.report_issues(issues)
