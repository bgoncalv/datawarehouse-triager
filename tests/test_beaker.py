"""Test triager."""
from dataclasses import dataclass
import unittest

import responses

from triager.beaker import BeakerChecker


class TestBeakerChecker(unittest.TestCase):
    """Test BeakerChecker."""

    def setUp(self):
        self.beaker_checker = BeakerChecker('https://beaker')

    @responses.activate
    def test_get_recipe(self):
        """Test get_recipe method."""
        responses.add(
            responses.GET, url='https://beaker/recipes/123',
            json={'test': 'ok'}
        )

        recipe = self.beaker_checker.get_recipe(123)
        self.assertEqual(recipe, {'test': 'ok'})

    @responses.activate
    def test_search_task_in_recipe(self):
        # pylint: disable=protected-access
        """Test _search_task_in_recipe method."""
        recipe = {
            'tasks': [
                {
                    'name': 'Random test',
                    'id': 106700541,
                    'recipe_id': 7945824,
                    't_id': 'T:106700541',
                },
                {
                    'name': 'Random test',
                    'id': 106700542,
                    'recipe_id': 7945824,
                    't_id': 'T:106700542',
                },
                {
                    'name': 'Random test',
                    'id': 106700543,
                    'recipe_id': 7945824,
                    't_id': 'T:106700543',
                },
            ]
        }

        self.assertEqual(
            recipe['tasks'][1],
            BeakerChecker._search_task_in_recipe(recipe, 106700542)
        )

        self.assertEqual(
            recipe['tasks'][2],
            BeakerChecker._search_task_in_recipe(recipe, 106700543)
        )

        self.assertEqual(
            None,
            BeakerChecker._search_task_in_recipe(recipe, 106700544)
        )

    @responses.activate
    def test_get_task(self):
        """Test get_task method."""
        recipe = {
            'tasks': [
                {
                    'name': 'Random test',
                    'id': 106700541,
                    'recipe_id': 7945824,
                    't_id': 'T:106700541',
                },
                {
                    'name': 'Random test',
                    'id': 106700542,
                    'recipe_id': 7945824,
                    't_id': 'T:106700542',
                },
                {
                    'name': 'Random test',
                    'id': 106700543,
                    'recipe_id': 7945824,
                    't_id': 'T:106700543',
                },
            ]
        }

        responses.add(
            responses.GET, url='https://beaker/recipes/7945824',
            json=recipe
        )

        self.assertEqual(
            recipe['tasks'][1],
            self.beaker_checker.get_task(7945824, 106700542)
        )

    def test_local_watchdog_expired(self):
        """Test local_watchdog_expired method."""
        task = {
            'name': 'Random test',
            'id': 106700549,
            'recipe_id': 7945824,
            'result': 'Warn',
            'results': [
                {'id': 491598185,
                 'message': None,
                 'path': 'Test',
                 'result': 'Pass'},
                {'id': 491598423,
                 'message': None,
                 'path': '/10_localwatchdog',
                 'result': 'Warn'}
            ],
            'status': 'Aborted',
            't_id': 'T:106700549'
        }

        self.assertTrue(BeakerChecker.local_watchdog_expired(task))
        self.assertTrue(BeakerChecker.watchdog_expired(task))

        # Remove /10_localwatchdog result.
        del task['results'][1]
        self.assertFalse(BeakerChecker.local_watchdog_expired(task))
        self.assertFalse(BeakerChecker.watchdog_expired(task))

    def test_local_watchdog_expired_task_not_warn(self):
        """Test local_watchdog_expired method. Task result is not Warn."""
        task = {
            'name': 'Random test',
            'id': 106700549,
            'recipe_id': 7945824,
            'result': 'Fail',
            'results': [
                {'id': 491598185,
                 'message': None,
                 'path': 'Test',
                 'result': 'Pass'},
                {'id': 491598423,
                 'message': None,
                 'path': '/10_localwatchdog',
                 'result': 'Warn'}
            ],
            'status': 'Aborted',
            't_id': 'T:106700549'
        }

        self.assertFalse(BeakerChecker.local_watchdog_expired(task))
        self.assertFalse(BeakerChecker.watchdog_expired(task))

    def test_external_watchdog_expired(self):
        """Test external_watchdog_expired method."""
        task = {
            'name': 'Random test',
            'id': 106700549,
            'recipe_id': 7945824,
            'result': 'Warn',
            'results': [
                {'id': 491598185,
                 'message': None,
                 'path': 'Test',
                 'result': 'Pass'},
                {'id': 491598423,
                 'message': 'External Watchdog Expired',
                 'path': '/',
                 'result': 'Warn'}
            ],
            'status': 'Aborted',
            't_id': 'T:106700549'
        }

        self.assertTrue(BeakerChecker.external_watchdog_expired(task))
        self.assertTrue(BeakerChecker.watchdog_expired(task))

        # Remove External Watchdog Expired result.
        del task['results'][1]
        self.assertFalse(BeakerChecker.external_watchdog_expired(task))
        self.assertFalse(BeakerChecker.watchdog_expired(task))

    def test_external_watchdog_expired_boottest(self):
        """Test external_watchdog_expired method on boot test."""
        task = {
            'name': 'Boot test',
            'id': 106700549,
            'recipe_id': 7945824,
            'result': 'Warn',
            'results': [
                {'id': 491598185,
                 'message': None,
                 'path': 'Test',
                 'result': 'Pass'},
                {'id': 491598423,
                 'message': 'External Watchdog Expired',
                 'path': '/',
                 'result': 'Warn'}
            ],
            'status': 'Aborted',
            't_id': 'T:106700549'
        }

        self.assertFalse(BeakerChecker.external_watchdog_expired(task))
        self.assertFalse(BeakerChecker.watchdog_expired(task))

    @responses.activate
    def test_check_testrun(self):
        """Test check_testrun method."""
        @dataclass
        class TestMock:
            """Mock TestRun."""
            misc: dict

        recipe = {
            'tasks': [
                {
                    'name': 'Random test',
                    'id': 106700549,
                    'recipe_id': 7945824,
                    'result': 'Warn',
                    'results': [
                        {'id': 491598185,
                         'message': None,
                         'path': 'Test',
                         'result': 'Pass'},
                        {'id': 491598423,
                         'message': 'External Watchdog Expired',
                         'path': '/',
                         'result': 'Warn'}
                    ],
                    'status': 'Aborted',
                    't_id': 'T:106700549'
                }
            ]
        }

        responses.add(
            responses.GET, url='https://beaker/recipes/7945824',
            json=recipe
        )

        testrun = TestMock({'beaker': {'recipe_id': 7945824, 'task_id': 106700549}})

        self.assertTrue(
            self.beaker_checker.check_testrun(testrun)
        )
