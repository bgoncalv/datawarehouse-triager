"""Test regexes."""
from dataclasses import dataclass
import unittest

from datawarehouse import Datawarehouse
import responses

from settings import NOT_FOUND
from triager import regexes


@dataclass
class TestMock:
    """Mock TestRun."""
    name: str
    description: str


@unittest.mock.patch('triager.regexes.dw_client', Datawarehouse('http://datawarehouse'))
class TestRegexChecker(unittest.TestCase):
    """Test TestRegexChecker."""

    def setUp(self):
        """setUp."""
        self.checker = regexes.RegexChecker()

    @responses.activate
    def test_search(self):
        """Test search."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": None
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/regex',
                      json={'results': {'issue_regexes': [lookup]}})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        self.assertEqual(
            [{'name': 'Bug description', 'id': 64}],
            self.checker.search(
                text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

    @responses.activate
    def test_search_no_result(self):
        """Test search with no result."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": None
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/regex',
                      json={'results': {'issue_regexes': [lookup]}})

        self.checker.download_lookups()

        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                "Text with no matches.",
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

    @responses.activate
    def test_search_test_name(self):
        """Test search with test_name."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": "test-name"
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/regex',
                      json={'results': {'issue_regexes': [lookup]}})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

        self.assertEqual(
            [{'name': 'Bug description', 'id': 64}],
            self.checker.search(
                text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('test-path', 'Test with test-name in the name.')
            )
        )

    @responses.activate
    def test_search_file_name(self):
        """Test search with file_name."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": 'file.name',
            "test_name_match": None
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/regex',
                      json={'results': {'issue_regexes': [lookup]}})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

        self.assertEqual(
            [{'name': 'Bug description', 'id': 64}],
            self.checker.search(
                text,
                {'name': 'test_file.name', 'url': 'http://server/test_file.name'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

    @responses.activate
    def test_search_file_name_and_test_name(self):
        """Test search with file_name and test_name."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": 'file.name',
            "test_name_match": 'test-name'
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/regex',
                      json={'results': {'issue_regexes': [lookup]}})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""

        # test_name and file_name wrong, text ok.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

        # test_name ok, file_name wrong, text ok.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('stress-ng', 'Test with test-name in the name.')
            )
        )

        # test_name wrong, file_name ok, text ok.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                text,
                {'name': 'file.name', 'url': 'http://server/file.name'},
                TestMock('stress-ng', 'stress-ng')
            )
        )

        # test_name ok, file_name ok, text wrong.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                'wrong text',
                {'name': 'file.name', 'url': 'http://server/file.name'},
                TestMock('test-path', 'Test with test-name in the name.')
            )
        )

        # All ok.
        self.assertEqual(
            [{'name': 'Bug description', 'id': 64}],
            self.checker.search(
                text,
                {'name': 'file.name', 'url': 'http://server/file.name'},
                TestMock('test-path', 'Test with test-name in the name.')
            )
        )

    @responses.activate
    def test_search_regex_syntax(self):
        """Test search with some regex syntax."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": "some.*thing|console.log",
            "test_name_match": "this-name|other-name",
        }

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/regex',
                      json={'results': {'issue_regexes': [lookup]}})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        self.assertEqual(
            [{'name': 'Bug description', 'id': 64}],
            self.checker.search(
                text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('this-name', 'this-name')
            )
        )

        self.assertEqual(
            [{'name': 'Bug description', 'id': 64}],
            self.checker.search(
                text,
                {'name': 'console.log', 'url': 'http://server/console.log'},
                TestMock('other-name', 'other-name')
            )
        )

        self.assertEqual(
            [{'name': 'Bug description', 'id': 64}],
            self.checker.search(
                text,
                {'name': 'something', 'url': 'http://server/something'},
                TestMock('other-name', 'other-name')
            )
        )

        self.assertEqual(
            [{'name': 'Bug description', 'id': 64}],
            self.checker.search(
                text,
                {'name': 'somebleblething', 'url': 'http://server/somebleblething'},
                TestMock('other-name', 'other-name')
            )
        )
