"""Test triager."""
import unittest

from datawarehouse import Datawarehouse
from mock import patch
import responses

from settings import FAIL_KICKSTART
from tests import mock_responses
from triager.checkers import FailureChecker
from triager.checkers import TestFailureChecker
from triager.triager import DWObject


class FailureCheckerTest(unittest.TestCase):
    """Test FailureChecker."""

    @responses.activate
    @patch('triager.regexes.dw_client', Datawarehouse('http://datawarehouse'))
    def test_check_logs_with_regex(self):
        """Test check_logs_with_regex."""
        responses.add(responses.GET, url='https://logs/console.log',
                      body=(b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for '
                            b'bfq_bfqq_move '))
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/regex',
                      json={'results': {'issue_regexes': mock_responses.lookup_issues}})
        test = DWObject('test', {
            'build_id': 'redhat:952371',
            'id': 'redhat:113990600',
            'path': 'boot',
            'description': 'Boot test',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/foobar', 'name': '8704397_aarch64_2_foobar'},
                {'url': 'https://logs/console.log',
                 'name': '8704397_aarch64_2_systemd_journal.log'},
            ]
        })

        issues = FailureChecker(test).check_logs_with_regex()
        self.assertEqual(
            [{'name': 'Bug description', 'id': 88}],
            issues)

        # Logs have no info.
        responses.replace(responses.GET, url='https://logs/console.log',
                          body=b'2019-11-17 07:15:09,105   ')
        issues = FailureChecker(test).check_logs_with_regex()
        self.assertEqual([], issues)

    @responses.activate
    @patch('triager.regexes.dw_client', Datawarehouse('http://datawarehouse'))
    def test_check_logs_with_regex_multiple_matches(self):
        """Test check_logs_with_regex. More than one regex matches."""
        responses.add(responses.GET, url='https://logs/console.log',
                      body=(b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for '
                            b'bfq_bfqq_move '))
        responses.add(responses.GET, url='https://logs/syscalls.fail.log',
                      body=(b'[   41.451946] tag=kcmp03 error error FAIL: clone'))

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/regex',
                      json={'results': {'issue_regexes': mock_responses.lookup_issues}})
        test = DWObject('test', {
            'build_id': 'redhat:952371',
            'id': 'redhat:113990600',
            'path': 'ltp',
            'description': 'LTP',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/console.log',
                 'name': '8704397_aarch64_2_systemd_journal.log'},
                {'url': 'https://logs/syscalls.fail.log',
                 'name': '8704397_aarch64_2_syscalls.fail.log'},
            ]
        })

        issues = FailureChecker(test).check_logs_with_regex()
        self.assertEqual(
            [
                {'id': 88, 'name': 'Bug description'},
                {'id': 126, 'name': 'BUG: clone.*failing after kernel commit'}
            ],
            issues
        )

    @patch('triager.checkers.FailureChecker.check_logs_with_regex')
    def test_check_check_all(self, check1):
        """Test check_all call."""
        issues = FailureChecker(None)
        issues.check_all()

        self.assertTrue(check1.called)

    @patch('triager.checkers.FailureChecker.check_all')
    def test_check_check(self, check_all):
        """Test check call."""
        FailureChecker.check(None)

        self.assertTrue(check_all.called)


class TestFailureCheckerTest(unittest.TestCase):
    """Test TestFailureChecker."""

    def test_check_kickstart_error(self):
        """Test check_kickstart_error."""
        test = DWObject('test', {
            'build_id': 'redhat:952371',
            'id': 'redhat:113990600',
            'path': 'boot',
            'description': 'Boot test',
            'duration': 0,
        })
        issues = TestFailureChecker(test).check_kickstart_error()

        self.assertEqual([FAIL_KICKSTART], issues)

    def test_check_kickstart_ok(self):
        """Test check_kickstart_error."""
        test = DWObject('test', {
            'build_id': 'redhat:952371',
            'id': 'redhat:113990600',
            'path': 'boot',
            'description': 'Boot test',
            'duration': 10,
        })
        issues = TestFailureChecker(test).check_kickstart_error()

        self.assertEqual([], issues)

    @responses.activate
    @patch('triager.regexes.dw_client', Datawarehouse('http://datawarehouse'))
    @patch('triager.beaker.BeakerChecker.check_testrun')
    def test_check_beaker_watchdogs(self, mock_check_testrun):
        """Test check_beaker_watchdogs."""
        test = DWObject('test', {
            'build_id': 'redhat:952371',
            'id': 'redhat:113990600',
            'path': 'ltp',
            'description': 'LTP',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/console.log',
                 'name': '8704397_aarch64_2_systemd_journal.log'},
                {'url': 'https://logs/syscalls.fail.log',
                 'name': '8704397_aarch64_2_syscalls.fail.log'},
            ],
            'misc': {
                'beaker': {
                    'task_id': 2,
                    'recipe_id': 1,
                }
            }
        })

        # Watchdog expired.
        mock_check_testrun.return_value = True
        issues = TestFailureChecker(test).check_beaker_watchdogs()
        self.assertEqual(
            [{'id': 36, 'name': 'Watchdog Expired'}],
            issues
        )

        # No watchdog expired.
        mock_check_testrun.return_value = False
        issues = TestFailureChecker(test).check_beaker_watchdogs()
        self.assertEqual([], issues)

        # Not a beaker test
        del test.attrs['misc']['beaker']
        issues = TestFailureChecker(test).check_beaker_watchdogs()
        self.assertEqual([], issues)

    @patch('triager.checkers.TestFailureChecker.check_logs_with_regex')
    @patch('triager.checkers.TestFailureChecker.check_kickstart_error')
    @patch('triager.checkers.TestFailureChecker.check_beaker_watchdogs')
    def test_check_check_all(self, check1, check2, check3):
        """Test check_all call."""
        issues = TestFailureChecker(None)
        issues.check_all()

        self.assertTrue(check1.called)
        self.assertTrue(check2.called)
        self.assertTrue(check3.called)
