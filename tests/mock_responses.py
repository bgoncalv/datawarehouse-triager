"""Responses data."""
# flake8: noqa: E501
# pylint: disable-all
lookup_issues = [
    {
        "id": 2,
        "issue": {
            "id": 88,
            "kind": {
                "id": 1,
                "description": "Kernel bug",
                "tag": "Kernel Bug"
            },
            "description": "Bug description",
            "ticket_url": "https://bug.link",
            "resolved": False,
            "generic": False
        },
        "text_match": "bnx2x .* Direct firmware load for",
        "file_name_match": None,
        "test_name_match": None
    },
    {
        "id": 15,
        "issue": {
            "id": 126,
            "kind": {
                "id": 1,
                "description": "Kernel bug",
                "tag": "Kernel Bug"
            },
            "description": "BUG: clone.*failing after kernel commit",
            "ticket_url": "http://url.com",
            "resolved": False,
            "generic": False,
            "origin_tree": None
        },
        "text_match": "tag=kcmp03.*FAIL: clone",
        "file_name_match": "syscalls.fail.log",
        "test_name_match": "LTP"
    }
]
